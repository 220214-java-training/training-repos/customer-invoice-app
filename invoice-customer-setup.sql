/*
 *  SQL Script
 */
drop table if exists invoice;
drop table if exists customer;


create table customer(
	customer_id serial primary key,
	first_name varchar(50),
	last_name varchar(50),
	email varchar(100) not null unique,
	birthday date
);

create table invoice(
	invoice_id serial primary key,
	purchase_date date,
	total numeric(8,2),
	customer_id integer references customer
);

insert into customer (first_name,last_name,email,birthday) values ('Sally','Jenkins','sallyjenkins@gmail.com','2022-01-20');
insert into customer (first_name,last_name,email,birthday) values ('Lola','Smith','lsmith@gmail.com','1997-06-09');
insert into customer (first_name,last_name,email,birthday) values ('Paul','McKenzie','pmck@gmail.com','1863-10-07');
insert into customer (first_name,last_name,email,birthday) values ('Charlie','Wislon','cwilson@gmail.com','1959-09-04');
--insert into customer (first_name,last_name,email,birthday) values ('Doug','Bradley','dbrad@gmail.com','1991-11-19');


insert into invoice (purchase_date,total,customer_id) values ('2022-01-20',87.54,2);
insert into invoice (purchase_date,total,customer_id) values ('2022-01-25',16.25,2);
insert into invoice (purchase_date,total,customer_id) values ('2022-01-21',34.20,3);



