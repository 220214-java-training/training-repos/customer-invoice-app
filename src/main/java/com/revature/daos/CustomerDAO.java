package com.revature.daos;

import com.revature.models.Customer;

import java.time.LocalDate;
import java.util.List;

public interface CustomerDAO {

    /*
        CRUD Operations

        - create a new customer record in db
        - read customer data
        - update customer information
        - delete customer from db
     */

    // creating a new customer
//    public void create(int id, String first, String last, String email, LocalDate birthday);
    public boolean create(Customer customer);

    // getting customer(s) data from the database
    public List<Customer> getAllCustomerData();





}
