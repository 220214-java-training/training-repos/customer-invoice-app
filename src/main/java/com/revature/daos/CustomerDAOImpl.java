package com.revature.daos;

import com.revature.models.Customer;
import com.revature.util.ConnectionUtil;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO{

    @Override
    public boolean create(Customer customer) {
        // validate user input
        if(customer.getEmail()==null || customer.getEmail().isEmpty()){
            return false;
        }

        // use customer object to create valid SQL statement
        // insert into customer values (); this statement is parameterized so we want to use a PS
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement("insert into customer values (default, ?, ?, ?, ?)");){

            ps.setString(1,customer.getFirstName());
            ps.setString(2, customer.getLastName());
            ps.setString(3, customer.getEmail());
            ps.setObject(4,customer.getBirthday());

            // provide that SQL to JDBC to execute in our DB
            int rowsAffected = ps.executeUpdate(); // returns the number of rows affected
            if(rowsAffected==1){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Customer> getAllCustomerData() {
        // create a connection to the database (using try with resources bc connection is Autocloseable)
        try (Connection connection = ConnectionUtil.getConnection();
             // create a statement (set params if we need to)
             // select * from customer
             Statement statement = connection.createStatement();){

            // execute statement, get a resultset in return
            ResultSet resultSet = statement.executeQuery("select * from customer");

            // creating list to store my customers when I get them from the db
            List<Customer> customers = new ArrayList<>();

            // create customer objects with what we get from the database (resultset)
                // get all of the fields from the records in our db and use them to populate a customer object
            while(resultSet.next()){
                // get all data from the row
                int id = resultSet.getInt("customer_id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                LocalDate birthday = resultSet.getObject("birthday", LocalDate.class);
                Customer c = new Customer(id, firstName, lastName, email, birthday);

                // add customer objects to a list
                customers.add(c);
            }

            // return list
            return customers;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
