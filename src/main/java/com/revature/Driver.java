package com.revature;

import com.revature.daos.CustomerDAO;
import com.revature.daos.CustomerDAOImpl;
import com.revature.models.Customer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public class Driver {

    public static void main(String[] args) {
        CustomerDAO customerDAO = new CustomerDAOImpl();
        //Customer c = new Customer("Doug", "Bradley", "dbrad@gmail.com", LocalDate.of(1991,11,19));
        //boolean isSuccessful = customerDAO.create(c);
        //System.out.println(isSuccessful);

        List<Customer> customerList = customerDAO.getAllCustomerData();
        System.out.println(customerList);

    }
}
